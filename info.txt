========================================================
==========================Info==========================
========================================================
Можно использовать как NoSQL. (RabbitMQ не читается из одного топика в несколько коньсюмеров. Нет буферизации пакетов).
С одним consumer работает один поток! поэтому лучше создавать много partition.
Несколько partition на одного consumer.
Топики создаются автоматически.
С одним partition работает однавременно 1-н consumer.
Запись ведется лидер - ведомый.
Данные обрабатываются поблочно.
Есть тайм индекс (для отмотки на определенное время).
TTL кафки 1 неделя (партиции будут удалятся постепенно(запись партиций идет шаг за шагом).
========================================================
*Идемпотентность (не обрабытывать запросы содержащие одни и теже данные) - с математики, на одни и теже данные возвращается одинаковый ответ.
  Уникальный идентификатор запроса.
  Проверка что идентификатор ще не обработан.
  Возвращаем старый результат обработки.
  В простейшем случае в качестве идентификатора можно использовать смещение сообщния в Кафке.
  В качестве идентификатора можно использовать хэш.
========================================================
Сравнение с RabbiMQ:
  RabbitMQ не читает из одного топика в несколько коньсюмеров.
  Нет буферизации пакетов.
========================================================
Default:
Создаеются 1 партиция и 1-ин топик при запросе;
batch.size = 16384 KB;
linger.ms = 25ms; (Задержка)
offsets.retention.minutes = 10080; Сколько хранить офсеты, до которых дочитали в прошлый раз.

========================================================
====================Setting up MySQL====================
========================================================
Сервер Mysql ID:
   |_SELECT @@server_id as SERVER_ID;
Создаем пользователя
   |_CREATE USER 'report'@'%' IDENTIFIED BY 'password';
Даем разрешения:
   |_GRANT SELECT, RELOAD, SHOW DATABASES, REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO 'report';
      |_FLUSH PRIVILEGES;
Проверяем binlig:
   |_SELECT variable_value as "BINARY LOGGING STATUS (log-bin) ::"
      FROM performance_schema.global_variables WHERE variable_name='log_bin'.

========================================================
========================Connector=======================
========================================================
Команды Connector:
    |_Добавить новый коннектор:
       |_curl -X POST -H "Accept:application/json" -H "Content-Type:application/json" http://kafka.net:8083/connectors -d @connect-mysql-broker-sync.json
       |_curl  --user USER:USERPASS -X POST -H "Accept:application/json" -H "Content-Type:application/json" http://kafka.net:8083/connectors -d @CONNECTOR.json
    |_Список коннекторов:
       |_curl -X GET http://kafka.net:8083/connectors/ | jq
       |_curl  --user USER:USERPASS -X GET http://kafka.net:8083/connectors/ | jq
    |_Статус коннектора:
       |_curl -X GET http://kafka.net:8083/connectors/connect-mysql-broker-sync/status | jq
       |_docker exec -ti kafka bash -c "curl  --user USER:USERPASS -X GET http://kafka.net:8083/connectors/" | jq
    |_Удалить коннектор:
       |_curl -X DELETE -H "Accept:application/json" -H "Content-Type:application/json" http://kafka.net:8083/connectors/CONNECTOR
       |_curl --user USER:USERPASS -X DELETE -H "Accept:application/json" -H "Content-Type:application/json" http://kafka.net:8083/connectors/CONNECTOR

========================================================
=========================Broker=========================
========================================================
Команды Broker:
   |_Список topic`s:
      |_docker exec -ti kafka kafka-topics \
       --bootstrap-server kafka:9092 \
       --command-config /opt/sasl/adminclient-configs.conf \
       --list

   |_Изменить топик:
      |_docker exec -ti kafka kafka-topics \
       --bootstrap-server kafka1.net:9092, kafka2.net:9092, kafka3.net:9092 \
       --command-config /opt/sasl/adminclient-configs.conf \
       --topic TOPICNAME \
       --alter \
       --partitions 40

   |_Удалить топик topic:
      |_docker exec -ti kafka kafka-topics \
       --bootstrap-server kafka1.net:9092, kafka2.net:9092, kafka3.net:9092 \
       --command-config /opt/sasl/adminclient-configs.conf \
       --topic TOPICNAME \
       --delete

   |_Информация о топике:
      |_docker exec -ti kafka kafka-topics \
       --bootstrap-server kafka1.net:9092, kafka2.net:9092, kafka3.net:9092 \
       --command-config /opt/sasl/adminclient-configs.conf \
       --topic TOPICNAME \
       --describe

   |_Просмотреть запись в топик:
      |_docker exec -ti kafka kafka-console-consumer \
       --bootstrap-server kafka1.net:9092, kafka2.net:9092, kafka3.net:9092 \
       --consumer.config /opt/sasl/adminclient-configs.conf \
       --topic TOPICNAME \
       --offset 0 --partition 0 \
       --property print.key=true

   |_Информация кластере:
      |_docker exec -ti kafka kafka-broker-api-versions \
       --bootstrap-server kafka1.net:9092, kafka2.net:9092, kafka3.net:9092 \
       --command-config /opt/sasl/adminclient-configs.conf

   |_Удалить broker (id смотрим в команде выше):
      |_docker exec -ti kafka kafka-remove-brokers \
       --bootstrap-server kafka1.net:9092, kafka2.net:9092, kafka3.net:9092 \
       --command-config /opt/sasl/adminclient-configs.conf \
       --broker-id 1 \
       --delete 1>&2 | grep -v SLF4J

   |_Список consumer group:
      |_docker exec -ti kafka kafka-consumer-groups \
       --bootstrap-server kafka1.net:9092, kafka2.net:9092, kafka3.net:9092 \
       --command-config /opt/sasl/adminclient-configs.conf \
       --list

   |_Команды ACL:
       |_Список ACL:
          |_docker exec -ti kafka1 kafka-acls \
           --bootstrap-server kafka1.net:9092, kafka2.net:9092, kafka3.net:9092 \
           --command-config /opt/sasl/adminclient-configs.conf \
           --list \

       |_Создать BY admin user:
          |_ddocker exec -ti kafka kafka-acls \
           --bootstrap-server kafka.net:9092 \
           --command-config /opt/sasl/adminclient-configs.conf \
           --add \
           --allow-principal User:* \
           --operation ALL \
           --allow-host '*' \
           --group  '*' \
           --topic '*' \
           --cluster

========================================================
=======================Zookeeper========================
========================================================
Команды Zookeeper:
    |_Информация о кластере
       |_docker exec -ti zookeeper bash -c "echo stat | nc localhost 2281"
    |_Подробная информация
       |_docker exec -ti zookeeper bash -c "echo mntr | nc localhost 2281"

    |_docker exec -ti zookeeper bash
          |_zkCli.sh -server localhost:2281
             |_Список kafka brokers:
                 |_ls /brokers/ids
                   Output:
                      |_c[1, 2, 3]
             |_Список kafka topics:
                 |_ls /brokers/topics

             |_Подробная информация о kafka broker id 1:
                 |_get /brokers/ids/1

             |_Проверка состояния:
                 |_echo state | nc zookeeper.net 2281
                 |_echo mntr | nc zookeeper.net 2281 | grep followers

========================================================
================РЕКОМЕНДУЕМАЯ НАСТРОЙКА:================
========================================================
  KAFKA_CFG_OFFSETS_TOPIC_REPLICATION_FACTOR: 3     #Critical cfg files (old param 1)
  KAFKA_CFG_TRANSACTION_STATE_LOG_MIN_ISR: 2     #Critical cfg files (old param 2)
  KAFKA_CFG_TRANSACTION_STATE_LOG_REPLICATION_FACTOR: 3     #Critical cfg files (old param 1)
========================================================
CONNECTOR:
  OFFSET_FLUSH_INTERVAL_MS=60000
  OFFSET_FLUSH_TIMEOUT_MS=5000
  SHUTDOWN_TIMEOUT=10000
========================================================
Сводим потерю сообщения к минимуму:
  min.insync.replicas = 1    Пока на 1 реплику ответ не доедет
  unclean.leader.election.enable = false     Не выбирать отстающего лидером
========================================================
Подтверждение:
  retries = 60  отправить 60раз
  retry.backoff.ms = 10000 на протяжени 10сек
  только потом удалить
========================================================
Управляет сессией коньсюмеров:
  max.pool.interval=10min
  session.timeout.ms=2h
  group.instance.id
